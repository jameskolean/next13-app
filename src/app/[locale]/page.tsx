import styles from "./page.module.css";
import pick from "lodash/pick";
import { NextIntlClientProvider, useLocale, useTranslations } from "next-intl";
import Alert from "@/components/client/Alert";
import Chip from "@/components/server/Chip";

export default function Home() {
  const t = useTranslations("Index");
  const alertTransations = useTranslations("Alert");
  return (
    <main className={styles.main}>
      <h1>Welcome to Next 13 App</h1>
      <h2>{t("title")}</h2>
      <Alert title={alertTransations("alert-title")}>
        <p>{alertTransations("alert-description")}</p>
      </Alert>
      <Chip />
    </main>
  );
}
