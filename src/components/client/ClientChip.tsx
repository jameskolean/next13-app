"use client";

import { ReactNode } from "react";
import { useTranslations } from "next-intl";

// Example passing translator to a client component
export default function ClientChip() {
  const t = useTranslations("Chip");
  return (
    <div>
      <h2> {t("title")}</h2>
    </div>
  );
}
